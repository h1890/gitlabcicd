FROM gradle:6.9-jdk11-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build -x test --no-daemon


FROM openjdk:11
COPY build/libs/springbootaws-0.0.1-SNAPSHOT.jar springbootaws-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","springbootaws-0.0.1-SNAPSHOT.jar"]
