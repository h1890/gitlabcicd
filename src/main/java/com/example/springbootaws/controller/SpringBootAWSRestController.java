package com.example.springbootaws.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBootAWSRestController {

    @GetMapping("/hello")
    public String getHelloString()
    {
        return "Hello amit with container";
    }
}
